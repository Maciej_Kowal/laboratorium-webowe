<!DOCTYPE html>
<html>
 <head>
 <meta charset = "utf-8">
 <title>Menedżer</title>
 <meta name="description" content="Menadżer">
    <meta name="keywords" content="Menadżer">
 <link rel = "stylesheet" type = "text/css"href = "styles.css">
 
 </head>
<body>
 
<div id = "pojemnik">
<div id = "tytul">Menadżer</div>
<div id= "strona">

<?php
$testString = "3.5 seconds";
$testDouble = 79.2;
$testInteger = 12;
?>
<p>Original values:</p>
<?php
print( "<p>$testString is a(n) " . gettype( $testString ). "</p>" );
print( "<p>$testDouble is a(n) " . gettype( $testDouble ) . "</p>" );
print( "<p>$testInteger is a(n) " . gettype( $testInteger ) . "</p>" );
?>
<p class>Converting to other data types:</p>
<?php
print( "<p>$testString " );
settype( $testString, "double" );
print( " as a double is $testString</p>" );
print( "<p>$testString " );
settype( $testString, "integer" );
print( " as an integer is $testString</p>" );
settype( $testString, "string" );
print( "<p class = 'space'>Converting back to a string results in $testString</p>" );
$data = "98.6 degrees";
print( "<p class = 'space'>Before casting: $data is a " . gettype( $data ). "</p>" );
print( "<p class = 'space'>Using type casting instead:</p> <p>as a double: ". (double) $data ."</p>" . "<p>as an integer: " . (integer) $data . "</p>");
print( "<p class = 'space'>After casting: $data is a " . gettype( $data ) . "</p>" );

?>
<form action = "wynagrodzenia.php" method ="post">
Pracownik 1:
<input type = "text" name = "dane1"/> Liczba godzin: 
<input type = "text" name = "godziny1"/> <br />
Pracownik 2:
<input type = "text" name = "dane2"/> Liczba godzin:
<input type = "text" name = "godziny2"/> <br />
Pracownik 3:
<input type = "text" name = "dane3"/> Liczba godzin:
<input type = "text" name = "godziny3"/> <br />
Zdanie
<input type = "text" name = "zdanie"/> <br />
Podmień
<input type = "text" name = "pod"/> <br />
Na
<input type = "text" name = "na"/> <br />
<input type = "submit" value = "wylicz" />


</form>
</div>
</div>
</div>
</body>
</html>