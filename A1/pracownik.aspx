﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pracownik.aspx.cs" Inherits="A1.pracownik" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <link rel = "stylesheet" type = "text/css"href = "style.css">
</head>
<body style="height: 250px">
    <div id="pojemnik">
    <form id="form1" runat="server">
        <div>
            <asp:Label ID="LabelLogin" runat="server" Text="Login"></asp:Label>
        </div>
        <asp:TextBox ID="TextBoxLogin" runat="server" ></asp:TextBox>
        <br />
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBoxLogin" ErrorMessage="Pole musi byc wypełnione"></asp:RequiredFieldValidator>
        <br />
        <p>
            <asp:Label ID="LabelHaslo1" runat="server" Text="Haslo:"></asp:Label>
        </p>
        <asp:TextBox ID="TextBoxHaslo1" runat="server" ></asp:TextBox>
        <br />
        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TextBoxHaslo1" ErrorMessage="Pole musi być wypełnione"></asp:RequiredFieldValidator>
        <p>
            <asp:Label ID="LabelHaslo2" runat="server" Text="Powtórz haslo:"></asp:Label>
        </p>
        <asp:TextBox ID="TextBoxHaslo2" runat="server" ></asp:TextBox>
        <br />
        <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="TextBoxHaslo1" ControlToValidate="TextBoxHaslo2" ErrorMessage="Hasła nie są takie same"></asp:CompareValidator>
        <br />
        <br />
        <asp:Label ID="LabelMail" runat="server" Text="Mail:"></asp:Label>
        <br />
        <asp:TextBox ID="TextBoxMail" runat="server" ></asp:TextBox>
        <br />
        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="TextBoxMail" ErrorMessage="Niepoprawny mail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
        <br />
        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="TextBoxMail" ErrorMessage="Pole musi być wypełnione"></asp:RequiredFieldValidator>
        <br />
        <br />
        <asp:Label ID="LabelRok" runat="server" Text="Rok urodzenia:"></asp:Label>
        <br />
        <asp:TextBox ID="TextBoxRok" runat="server"  ></asp:TextBox>
        <br />
        <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="TextBoxRok" ErrorMessage="Wiek z zakresu 18-26" MaximumValue="2000" MinimumValue="1992"></asp:RangeValidator>
        <br />
        <br />
        <br />
        <asp:Label ID="LabelWynik" runat="server"></asp:Label>
        <br />
        <asp:Label ID="LabelPonowny" runat="server"></asp:Label>
        <br />
        <asp:Button ID="ButtonWyslij" runat="server" OnClick="ButtonWyslij_Click" Text="Wyslij" />
       
    </form>
        
        </div>
</body>
</html>
