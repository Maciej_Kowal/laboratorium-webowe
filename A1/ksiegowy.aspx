﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ksiegowy.aspx.cs" Inherits="A1.ksiegowy" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <link rel = "stylesheet" type = "text/css"href = "style.css">
</head>
<body>
    <div id="pojemnik">
        <div id="pole"></div>
    <div id="pole2">
   <h1> Umowa zlecenia</h1> 
<p style ="font-size: 11pt; color: blue;" >
Przedmiotem <em>umowy zlecenia</em> jest wykonywanie określonej czynności prawnej innego podmiotu. Za wykonanie zlecenia zleceniobiorcy należy się wynagrodzenie. Zlecenie może być nieodpłatne, jednak brak wynagrodzenia powinien być zawarty w umowie. Przy braku dokładnego określenia wysokości wynagrodzenia należy się "wynagrodzenie odpowiadające wykonanej pracy", przy ustalaniu którego bierze się pod uwagę czas poświęcony na wykonanie zlecenia, stopień skomplikowania czynności będących przedmiotem zlecenia, przygotowanie zawodowe zleceniobiorcy (jego profesjonalizm). Zleceniobiorca ma obowiązek informować zleceniodawcę o przebiegu wykonywania umowy, a na zakończenie – przedstawić mu sprawozdanie z jej wykonania.

Umowę zlecenia określa się jako umowę starannego działania i przeciwstawia umowie o dzieło określanej umową rezultatu. Oznacza to, iż w umowie zlecenia ważna jest wykonywana praca (wykonywanie czynności) na rzecz zleceniodawcy, która niekoniecznie będzie prowadzić do określonego rezultatu – co jest przedmiotem umowy o dzieło. Wynagrodzenie z umowy zlecenia przysługuje za samo "staranne działanie", nie zaś za jego rezultat.

Od wynagrodzenia brutto pobierane są składki na ubezpieczenie społeczne i rentowe (nie są pobierane, jeśli zleceniobiorca ma status studenta) oraz składka na ubezpieczenie zdrowotne. Dodatkowo zleceniobiorca może na własne życzenie obciążyć swoje wynagrodzenie składką na ubezpieczenie chorobowe.

Od każdej umowy zlecenia należy odprowadzić zaliczkę na podatek dochodowy w wysokości 18% świadczenia pomniejszonego o koszty uzyskania przychodu, dodatkowo pomniejszoną o kwotę pobranej składki na ubezpieczenie zdrowotne nieprzekraczającej 7,75% podstawy wymiaru składki. W przypadku osoby, dla której umowa-zlecenie to jedyne zatrudnienie, koszty pracodawcy są takie same, jak przy umowie o pracę. Różnica pojawia się w momencie, gdy zleceniobiorca jest studentem do 26. roku życia lub osiąga w innej firmie dochód w wysokości równej lub wyższej od minimalnego wynagrodzenia – wtedy pracodawca nie odprowadza od zleceniodawcy swojej części składek do ZUS oraz na Fundusz Pracy i Fundusz Gwarantowanych Świadczeń Pracowniczych.
</p>
        </div>
        </div>
</body>
</html>
