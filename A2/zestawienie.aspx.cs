﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace A2
{
    public partial class zestawienie : System.Web.UI.Page
    {
        static Hashtable ht { get; set; } = new Hashtable();
        static int suma;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                
                foreach (var kucharz in lista.kucharze.Keys)
                {
                    if ((bool)lista.kucharze[kucharz])
                    {
                        ht.Add(kucharz, 0);
                    }
                }
                foreach (var kelner in lista.kelnerzy.Keys)
                {
                    if ((bool)lista.kelnerzy[kelner])
                    {
                        ht.Add(kelner, 0);
                    }
                }
                foreach (var barman in lista.barmani.Keys)
                {
                    if ((bool)lista.barmani[barman])
                    {
                        ht.Add(barman, 0);
                    }
                }

                CheckBoxList1.DataSource = ht.Keys;
                CheckBoxList1.DataBind();
                string ku = "";
                string ke = "";
                string ba = "";
                int pku = 0;
                int pke = 0;
                int pba = 0;
                 suma = 0;
                foreach (var kucharz in lista.kucharze.Keys)
                {
                    if ((bool)lista.kucharze[kucharz])
                    {
                        ku = (String)kucharz + " " + (int)lista.pracownicy[kucharz];
                        pku = pku + (int)lista.pracownicy[kucharz];
                        this.ListaKucharzy.Text = this.ListaKucharzy.Text + ku + "<br />";

                    }
                }
                foreach (var kelner in lista.kelnerzy.Keys)
                {
                    if ((bool)lista.kelnerzy[kelner])
                    {
                        ke = (String)kelner + " " + (int)lista.pracownicy[kelner];
                        pke = pke + (int)lista.pracownicy[kelner];
                        ListaKelnerow.Text = ListaKelnerow.Text + ke + "<br />";

                    }
                }
                foreach (var barman in lista.barmani.Keys)
                {
                    if ((bool)lista.barmani[barman])
                    {
                        ba = (String)barman + " " + (int)lista.pracownicy[barman];
                        pba = pba + (int)lista.pracownicy[barman];
                        ListaBarmanow.Text = ListaBarmanow.Text + ba + "<br />";

                    }
                }
                if (pku == 0)
                {
                    WynagrodzenieKucharzy.Text = "Brak pracujących kucharzy";
                }
                else
                {
                    WynagrodzenieKucharzy.Text = "Łączne wynagrodzenie kucharzy: " + pku.ToString();
                }
                if (pke == 0)
                {
                    WynagrodzenieKelnerow.Text = "Brak pracujących kelnerów";
                }
                else
                {
                    WynagrodzenieKelnerow.Text = "Łączne wynagrodzenie kelnerów: " + pke.ToString();
                }
                if (pba == 0)
                {
                    WynagrodzenieBarmanow.Text = "Brak pracujących barmanow";
                }
                else
                {
                    WynagrodzenieBarmanow.Text = "Łączne wynagrodzenie barmanów: " + pba.ToString();
                }
                 suma = pku + pke + pba;
                SumaWynagrodzen.Text = "Suma wynagrodzeń: " + suma.ToString();
                if (suma > 0)
                {
                    Button2.Enabled = true;
                    Stawka.Enabled = true;
                }
            }
        }
        
            protected void CheckBoxList1_SelectedIndexChanged(object sender, EventArgs e)
            {
            int stawka = 0;
            if (Stawka.Text != "")
            {
                try
                {
                    stawka = int.Parse(Stawka.Text);
                }
                catch
                {
                    stawka = 0;
                }
            }
            for (int i = 0; i < CheckBoxList1.Items.Count; i++)
                {
                    if (CheckBoxList1.Items[i].Selected)
                    {
                    
                        ht[CheckBoxList1.Items[i].Text] = stawka;
                    
                    }
                    else
                    {
                        ht[CheckBoxList1.Items[i].Text] = 0;
                    }
                }
                 }
        
        protected void Button1_Click(object sender, EventArgs e)
        {


            List<string> kku = lista.kucharze.Keys.Cast<string>().ToList();
            List<string> kke = lista.kelnerzy.Keys.Cast<string>().ToList();
            List<string> kba = lista.barmani.Keys.Cast<string>().ToList();
            foreach (string p in kku)
            {
                lista.kucharze[p] = false;
            }
            foreach (string p in kke)
            {
                lista.kelnerzy[p] = false;
            }
            foreach (string p in kba)
            {
                lista.barmani[p] = false;
            }
            Response.Redirect("lista.aspx");
            
               
            
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            Response.Redirect("podsumowanie.aspx");
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            int bonus = 0;
                  
                
            foreach (var p in ht.Keys)
            {
            bonus = bonus + (int)ht[p];
            }
            int razem = bonus + suma;
            Bonusy.Text = bonus.ToString();
            WynagrodzenieBonusy.Text = razem.ToString();
        }
    }
}