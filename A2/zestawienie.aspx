﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="zestawienie.aspx.cs" Inherits="A2.zestawienie" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <link rel = "stylesheet" type = "text/css"href = "style.css">
</head>
<body>
     <div id="pojemnik">

<div id="pole"></div>
    <form id="form1" runat="server">
        <div>
            <p>Kucharze: </p>
            <asp:Label ID="ListaKucharzy" runat="server"></asp:Label>
            <asp:Label ID="WynagrodzenieKucharzy" runat="server"></asp:Label>
        <p>Kelnerzy:</p>
        <asp:Label ID="ListaKelnerow" runat="server"></asp:Label>
        <asp:Label ID="WynagrodzenieKelnerow" runat="server"></asp:Label>
        <br /> 
            <p>Barmani:</p>
        <asp:Label ID="ListaBarmanow" runat="server"></asp:Label>
        <asp:Label ID="WynagrodzenieBarmanow" runat="server"></asp:Label>
        <br /> 
            <p>Suma:</p>
        <asp:Label ID="SumaWynagrodzen" runat="server"></asp:Label>
            <br /><br />
            <asp:TextBox ID="Stawka" runat="server" Enabled="False"></asp:TextBox>
            <br />
            <asp:CheckBoxList ID="CheckBoxList1" runat="server" OnSelectedIndexChanged="CheckBoxList1_SelectedIndexChanged">
            </asp:CheckBoxList>
            <asp:Button ID="Button3" runat="server" OnClick="Button3_Click" Text="Przelicz" />
            <br />
            <asp:Label ID="Bonusy" runat="server"></asp:Label>
            <br />
            <asp:Label ID="WynagrodzenieBonusy" runat="server"></asp:Label>
        <br />
            </div>
        <asp:Button ID="Button1" runat="server" Text="Wstecz" OnClick="Button1_Click" />
           <asp:Button ID="Button2" runat="server" Text="Dalej" OnClick="Button2_Click" Enabled="False" />
    </form>
         
         </div>
   
</body>
</html>
