﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace A2
{
    public partial class lista : System.Web.UI.Page
    {
        public static Hashtable pracownicy = new Hashtable()
        {
            {"Kucharz1", 2000 },
            {"Kucharz2", 1850 },
            {"Kucharz3", 2050 },
            {"Kucharz4", 1900 },
            {"Kelner1", 1500 },
            {"Kelner2", 1600 },
            {"Kelner3", 1550 },
            {"Kelner4", 1650 },
            {"Barman1", 1700 },
            {"Barman2", 1750 },
            {"Barman3", 1800 },
            {"Barman4", 1850 },

        };

        public static Hashtable kucharze = new Hashtable()
            {
            {"Kucharz1", false },
            {"Kucharz2", false },
            {"Kucharz3", false },
            {"Kucharz4", false },
            };
        public static Hashtable kelnerzy = new Hashtable()
        {
            {"Kelner1", false },
            {"Kelner2", false },
            {"Kelner3", false },
            {"Kelner4", false },

        };
        public static Hashtable barmani = new Hashtable()
        {
            {"Barman1", false },
            {"Barman2", false },
            {"Barman3", false },
            {"Barman4", false }
        };

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CheckBoxList1.DataSource = kucharze.Keys;
                CheckBoxList2.DataSource = kelnerzy.Keys;
                CheckBoxList3.DataSource = barmani.Keys;
                CheckBoxList1.DataBind();
                CheckBoxList2.DataBind();
                CheckBoxList3.DataBind();
            }
        }
       

    protected void RadioButtonList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selectedValue = RadioButtonList1.SelectedValue;
           if(selectedValue == "Kucharz")
            {
                CheckBoxList1.Visible = true;
                CheckBoxList2.Visible = false;
                CheckBoxList3.Visible = false;
            }
           else if (selectedValue == "Kelner")
            {
                CheckBoxList2.Visible = true;
                CheckBoxList1.Visible = false;
                CheckBoxList3.Visible = false;
            }
            else if (selectedValue == "Barman")
            {
                CheckBoxList3.Visible = true;
                CheckBoxList1.Visible = false;
                CheckBoxList2.Visible = false;
            }
        }

        protected void CheckBoxList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            for (int i = 0; i < CheckBoxList1.Items.Count; i++)
            {
                if (CheckBoxList1.Items[i].Selected)
                {
                    kucharze[CheckBoxList1.Items[i].Text] = true;
                    
                }
                else
                {
                    kucharze[CheckBoxList1.Items[i].Text] = false;
                }
            }
        }
        protected void CheckBoxList2_SelectedIndexChanged(object sender, EventArgs e)
        {
            for (int i = 0; i < CheckBoxList2.Items.Count; i++)
            {
                if (CheckBoxList2.Items[i].Selected)
                {
                    kelnerzy[CheckBoxList2.Items[i].Text] = true;

                }
                else
                {
                    kelnerzy[CheckBoxList2.Items[i].Text] = false;

                }
            }
        }

        protected void CheckBoxList3_SelectedIndexChanged(object sender, EventArgs e)
        {
            for (int i = 0; i < CheckBoxList3.Items.Count; i++)
            {
                if (CheckBoxList3.Items[i].Selected)
                {
                    barmani[CheckBoxList3.Items[i].Text] = true;

                }
                else
                {
                    barmani[CheckBoxList3.Items[i].Text] = false;

                }
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Redirect("zestawienie.aspx");
        }
    }
}