﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace A2
{
    public partial class podsumowanie : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            List<string> kku = lista.kucharze.Keys.Cast<string>().ToList();
            List<string> kke = lista.kelnerzy.Keys.Cast<string>().ToList();
            List<string> kba = lista.barmani.Keys.Cast<string>().ToList();
            foreach (string p in kku)
            {
                lista.kucharze[p] = false;
            }
            foreach (string p in kke)
            {
                lista.kelnerzy[p] = false;
            }
            foreach (string p in kba)
            {
                lista.barmani[p] = false;
            }
            Response.Redirect("lista.aspx");
        }
    }
}